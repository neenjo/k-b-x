package connection;

import com.sun.net.ssl.HttpsURLConnection;
import configuration.applicationConfiguration;
import jdk.nashorn.api.scripting.JSObject;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.HttpURLConnection;

/**
 * Created by neenjo on 31/3/16.
 */
public class networkConnection {
    String serverAddress;
    public networkConnection(String string)
    {
        try {
            serverAddress="http://";
            applicationConfiguration applicationConfiguration=new applicationConfiguration();
            serverAddress=serverAddress.concat(applicationConfiguration.getServerAddress());
            serverAddress=serverAddress.concat(":"+applicationConfiguration.getNodejsPort());
            serverAddress=serverAddress.concat("/response");
            URL url=new URL(serverAddress);
            HttpURLConnection httpURLConnection=(HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestProperty("Content-Type", "application/json");

            OutputStreamWriter writer = new OutputStreamWriter(httpURLConnection.getOutputStream());
            writer.write(string);
            writer.flush();
            writer.close();

            BufferedReader reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
    String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            System.out.println(line);
                    } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
