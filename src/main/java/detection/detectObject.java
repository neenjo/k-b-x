package detection;
import configuration.applicationConfiguration;
import connection.networkConnection;
import org.json.JSONArray;
import org.opencv.core.*;
import org.opencv.highgui.VideoCapture;
import org.opencv.objdetect.CascadeClassifier;
import video.MyFrame;

/**
 * Created by neenjo on 13/3/16.
 */
public class detectObject {
    static {
        System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
    }
   public detectObject(applicationConfiguration applicationConfiguration) {
        try {
            VideoCapture cap = new VideoCapture(0);
            if (!cap.isOpened()) {
                System.exit(-1);
            }
            Mat image = new Mat();
            MyFrame frame = new MyFrame();
            frame.setVisible(true);
            String cascadeLocation=applicationConfiguration.getCascadeLocation();
            int frequency=applicationConfiguration.getfrequencyOfImageAnalysis();
            int cutOff=applicationConfiguration.getfrequencyOfSendingImage();
            JSONArray jsonArray=applicationConfiguration.getCascadeFile();
            String dataToBeSent="{\n" +
                    "\"input\":\"";

            //second loop

            int[] countOfDetected=new int[jsonArray.length()];

            int i=0;
            while (i<500) {
                // Read current camera frame into matrix
                cap.read(image);
                if(i%frequency==0)
                {
                    for (int j=0;j<jsonArray.length();j++) {
                        CascadeClassifier cascadeClassifier = new CascadeClassifier(cascadeLocation+jsonArray.getString(j)+".xml");
                        MatOfRect matOfRect = new MatOfRect();
                        cascadeClassifier.detectMultiScale(image, matOfRect);
                        countOfDetected[j]+=matOfRect.toArray().length;
                        if(matOfRect.toArray().length!=0)
                        System.out.println(String.format("Detected %s "+jsonArray.getString(j), matOfRect.toArray().length));
                        for (Rect rect : matOfRect.toArray()) {
                            Core.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
                                    new Scalar(0, 255, 0));
                        }
                    }

                }
                i++;



                // Render frame if the camera is still acquiring images
                if (image != null) {
                    frame.render(image);
                } else {
                    System.out.println("No captured frame -- camera disconnected");
                    break;
                }
            }
            for (int p = 0; p < jsonArray.length(); p++) {
                if (countOfDetected[p]>=cutOff)
                {
                    dataToBeSent=dataToBeSent.concat(jsonArray.getString(p)+",");
                }
            }
            dataToBeSent=dataToBeSent.concat("\"\n" +
                    "}");
            System.out.print("Result:"+dataToBeSent);
            networkConnection networkConnection=new networkConnection(dataToBeSent);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
