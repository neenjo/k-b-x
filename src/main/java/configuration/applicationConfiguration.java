package configuration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.Scanner;

/**
 * Created by neenjo on 17/3/16.
 */
public class applicationConfiguration {
    JSONObject jsonObject;
    public applicationConfiguration() {
        try {
            String content = new Scanner(new File("src/main/java/resources/configure.json")).useDelimiter("\\Z").next();
            jsonObject=new JSONObject(content);

        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getCascadeLocation(){
        return jsonObject.getString("cascadeLocation");

    }
    public JSONArray getCascadeFile(){
        return jsonObject.getJSONArray("cascadeFile");

    }
    public String getServerAddress(){
        return jsonObject.getString("serverAddress");
    }
    public String getNodejsPort(){
        return jsonObject.getString("nodejsPort");
    }
    public int getfrequencyOfImageAnalysis(){
        return jsonObject.getInt("frequencyOfImageAnalysis");
    }
    public int getfrequencyOfSendingImage(){
        return jsonObject.getInt("frequencyOfSendingImage");
    }



}
